from sqlalchemy import create_engine
from sqlalchemy import text


def create_db():
    engine = create_engine("postgresql+psycopg2://postgres:postgres@localhost/postgres", isolation_level="AUTOCOMMIT",
                           echo=False)

    sql_create_role = text("CREATE ROLE teamquestions WITH LOGIN NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE "
                           "NOREPLICATION;"), "Creating role (if not yet exists)", True
    sql_update_password = text("ALTER USER teamquestions WITH PASSWORD 'teamquestions';"), "Setting password", False
    sql_create_db = text("CREATE DATABASE teamquestions WITH OWNER = teamquestions ENCODING = 'UTF8' "
                         "TABLESPACE = pg_default CONNECTION LIMIT = -1 IS_TEMPLATE = False;"), \
        "Creating database (if not yet exists)", True

    sql_commands = [sql_create_role, sql_update_password, sql_create_db]

    try:
        with engine.connect() as connection:
            for sql in sql_commands:
                print(sql[1])

                try:
                    connection.execute(sql[0])
                except Exception as err:
                    if not sql[2]:
                        # Third parameter is "ignore", needed because there's no CREATE ROLE IF NOT EXISTS e.g.
                        raise err
    except Exception as err:
        print(f"Could not create the database:\n\n{err}")


if __name__ == "__main__":
    create_db()
