import os
import yaml as yaml

from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        config_folder = os.path.join(settings.BASE_DIR, "utils", "management", "config")
        vindevoy_pwd_yaml_file_name = os.path.join(config_folder, 'vindevoy_pwd.secret')

        with open(vindevoy_pwd_yaml_file_name, 'r') as file:
            vindevoy_pwd_yaml = yaml.load(file, Loader=yaml.FullLoader)

        password = vindevoy_pwd_yaml["password"]

        vindevoy = User.objects.get(id=2)
        vindevoy.set_password(password)
        vindevoy.save()

        print("Password changed for vindevoy")

        admin_group = Group.objects.get(id=1)
        admin_group.user_set.add(vindevoy)

        print("Vindevoy added to administrators")
