from rest_framework import viewsets

from questions.models import Question
from questions.serializers import QuestionSerializer


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by("-id")
    serializer_class = QuestionSerializer
    # TODO: permission_classes = [permissions.IsAuthenticated]
