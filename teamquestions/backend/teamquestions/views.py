from django.contrib.auth.models import User
from rest_framework import viewsets

from teamquestions.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter(id__gt=1).order_by("id")
    serializer_class = UserSerializer
    # TODO: permission_classes = [permissions.IsAuthenticated]
