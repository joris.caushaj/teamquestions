# TeamQuestions


## About

TeamQuestions is a project that will start out as a tool where you can ask questions.  The members of a team can answer the question by responding to it, the can comment any other responses, ...  The original poster of the question will be able to mark the question as answered, ...

In the future, it's also the goal to have modules like:

- bookmarks
- forum
- links
- newsletters
- pages
- tags
- tutorials
- events
- calendar
- tags

## Technology Stack

The application is based on:

- Python
- Django 
- Flet.dev

